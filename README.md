# `podman-manager`

Check for containers to be up and running.
This tool is intended to ensure certain containers are running and take the needed actions to start or create them.

Podman is daemon-less, so we can't mark a container to start automatically (e.g. on startup) w/o generating a `systemd` unit.

`systemd` unit generation works well but:

* Obviously, it's tied to `systemd` (which isn't a big deal for me, but on non-systemd distros this won't work)
* It doesn't create new containers
* Generated units are tied to a specific container (by ID). Also if instructed to use the container's name still uses ID in some fields.
* Doesn't follow the container state if managed via `podman`

So, `podman-manager` has the following abilities
* Unpause paused containers
* Start stopped containers
* Pull images (well, `podman` does this really)
* Create new containers from configuration (somehow like `docker-compose` but only with basic features)
* Run as a daemon checking containers state periodically
* Check state and do needed actions once (one-shot)

`podman-manager` doesn't make assumptions, state is directly queried via `podman` REST API.

In case you were asking, no, this is not Ansible. `podman-manager` only ensures specified containers are running, it doesn't update containers to match the configuration you provided (ports, command and so on). Configuration is only used in case of having to create containers.

## Works on

| Distro     | Init + LibC   | Podman version |
| ---------- | :-----------: | :------------- |
| Arch Linux | systemd+glibc | 2.2.1          |
| Void       | runit+musl    | 2.2.1          |

Did it work for you? Make a report or update this file via Merge Request

## Contribute

* Merge Requests are very welcome.
* Still not have a GitLab account? Send patches to `https://lists.sr.ht/~mrvik/inbox` mailing to [~mrvik/inbox@lists.sr.ht](mailto:~mrvik/inbox@lists.sr.ht).
* Do you have a different use case and need some features? Create an issue so we can address it.
* Found a bug? Congrats! Create an issue

Oh come on, I don't have a GitLab account, how do I "create an issue"? [send a mail here](incoming+mrvik-podman-manager-23239459-issue-@incoming.gitlab.com)
