package config

import (
	"os"

	"github.com/containers/podman/v2/pkg/specgen"

	"gopkg.in/yaml.v3"
)

// Config will be parsed into this struct
type Config struct {
	// Use container name as key
	Containers map[string]*ContainerConfig

	// In a near future we plan adding Pods here
}

// Each container may or must have those properties.
// This is not Ansible, so if Name exists, we won't try to make the container look like this config.
// Podman-manager will ensure container is up and running. That's all
// Parameters here are only used when creating containers (in case name doesn't exist). See podman-run(1) for possible values
// Those will be checked only if container needs to be created, so a bad config may fail silently unless needed
type ContainerConfig struct {
	// Container name. Each container **must** have a name so we can identify it
	// This is automatically set when needed (from key on map pointing here)
	Name string `yaml:"-"`
	// Basic container config (will be used in case we must create a container)
	// Important: Image name **must** be fully qualified
	Image      string
	Pod        string
	Entrypoint []string
	Command    []string
	Env        map[string]string
	Hostname   string
	Remove     bool
	// Network config
	PublishExposedPorts bool         `yaml:"publish-exposed-ports"`
	PortMappings        PortMappings `yaml:"port-mappings"`

	// Volumes as a slice of Name and Dest options
	// Volumes are presistend and won't be even if the container has "Remove" set
	Volumes []*specgen.NamedVolume
	// More things to be included here as needed, but for now, this is enough
}

type PortMappings []PortMapping

// Convenience method to copy port mappings to podman's struct
func (pms PortMappings) GetSpec() []specgen.PortMapping {
	if len(pms) == 0 {
		return nil
	}
	out := make([]specgen.PortMapping, len(pms))
	for i, pm := range pms {
		out[i] = specgen.PortMapping{
			HostIP:        pm.HostIP,
			ContainerPort: pm.ContainerPort,
			HostPort:      pm.HostPort,
			Range:         pm.Range,
			Protocol:      pm.Protocol,
		}
	}
	return out
}

// We have to add yaml tags here, so we copy this from podman lib
type PortMapping struct {
	HostIP        string `yaml:"host-ip"`
	ContainerPort uint16 `yaml:"container-port"`
	HostPort      uint16 `yaml:"host-port"`
	Range         uint16
	Protocol      string
}

func ReadConfig(fromPath string) (config Config, err error) {
	file, err := os.Open(fromPath)
	if err != nil {
		return
	}
	defer file.Close()
	decoder := yaml.NewDecoder(file)
	decoder.KnownFields(true)
	err = decoder.Decode(&config)
	return
}
