module gitlab.com/mrvik/podman-manager

go 1.15

require (
	github.com/containers/common v0.29.0
	github.com/containers/podman/v2 v2.2.1
	gitlab.com/mrvik/logger v0.0.0-20200624181218-1870fc7f22c4
	golang.org/x/sys v0.0.0-20201221093633-bc327ba9c2f0
	gopkg.in/yaml.v3 v3.0.0-20200615113413-eeeca48fe776
)
