package main

import (
	"context"
	"flag"
	"os"
	"os/signal"
	"sync"
	"time"

	lgr "gitlab.com/mrvik/logger/log"
	"gitlab.com/mrvik/podman-manager/config"
	"gitlab.com/mrvik/podman-manager/podman"

	"golang.org/x/sys/unix"
)

var (
	log = lgr.Create("main", nil, nil)
)

func main() {
	var (
		daemon             bool
		socket, configPath string
	)
	flag.BoolVar(&daemon, "daemon", false, "Run podman-manager as a daemon (check containers on config all the time)")
	flag.StringVar(&socket, "socket", "unix:/run/podman/podman.sock", "Exposed podman socket. Socket must be accessibe (rw) to the user running this")
	flag.StringVar(&configPath, "config", "/etc/podman-manager.yml", "Podman manager config path")
	flag.Parse()
	ctx, cancelFn := context.WithCancel(context.Background())
	var wg sync.WaitGroup
	wg.Add(1)
	go watchSignals(ctx, cancelFn, &wg)
	config, err := config.ReadConfig(configPath)
	if err != nil {
		panic(err)
	}
	mgr, err := podman.NewManager(ctx, socket)
	if err != nil {
		panic(err)
	}

	ticker := time.NewTicker(time.Second * 10)
loop:
	for {
		select {
		case <-ctx.Done():
			break loop
		case <-ticker.C:
		}
		err = mgr.DoContainerActions(config.Containers)
		if err != nil {
			log.Errorf("Error doing container actions: %s\n", err)
		}
		if !daemon {
			cancelFn()
		}
	}
	if err != nil {
		panic(err)
	}
	wg.Wait()
}

func watchSignals(ctx context.Context, cancel context.CancelFunc, wg *sync.WaitGroup) {
	defer wg.Done()
	defer cancel()
	channel := make(chan os.Signal, 2)
	signal.Notify(channel, unix.SIGINT, unix.SIGTERM)
	select {
	case <-channel:
	case <-ctx.Done():
	}
	log.Info("Exiting signals routine")
}
