package podman

import (
	"fmt"

	"github.com/containers/podman/v2/libpod/define"
	"github.com/containers/podman/v2/pkg/bindings/containers"
	"github.com/containers/podman/v2/pkg/specgen"

	lgr "gitlab.com/mrvik/logger/log"
	"gitlab.com/mrvik/podman-manager/config"
)

var (
	log = lgr.Create("podman", nil, nil)
)

// Useful pointers
var (
	truePtr = boolPtr(true)
)

func boolPtr(b bool) *bool {
	return &b
}

func (c *Manager) FilterActions(containerNames []string) (needStart, needCreate, needUnpause []string, err error) {
	if len(containerNames) == 0 {
		return
	}
	filter := map[string][]string{
		"name": containerNames,
	}
	result, err := containers.List(c.connCtx, filter, truePtr, nil, nil, nil, truePtr)
	if err != nil {
		return
	}
	checkedNames := make(map[string]struct{})
	for _, entity := range result {
		if len(entity.Names) == 0 {
			continue
		}
		name := entity.Names[0]
		checkedNames[name] = struct{}{}
		status, err := define.StringToContainerStatus(entity.State)
		if err != nil {
			break
		}
		switch status {
		case define.ContainerStateUnknown:
			log.Errorf("Container %s is in a unknown state, ignoring it", name)
		case define.ContainerStateRunning:
			//Nothing to do
		case define.ContainerStatePaused:
			needUnpause = append(needUnpause, name)
		default:
			needStart = append(needStart, name)
		}
	}
	for _, name := range containerNames {
		if _, checked := checkedNames[name]; !checked {
			needCreate = append(needCreate, name)
		}
	}
	return
}

func (c *Manager) StartContainers(names []string) (err error) {
	for _, name := range names {
		err = containers.Start(c.connCtx, name, nil)
		if err != nil {
			break
		}
	}
	return
}

func (c *Manager) UnpauseContainers(names []string) (err error) {
	for _, name := range names {
		err = containers.Unpause(c.connCtx, name)
		if err != nil {
			break
		}
	}
	return
}

func (c *Manager) CreateContainers(specs []config.ContainerConfig) error {
	// First we get images
	err := c.PullImages(specs)
	if err != nil {
		log.Warnf("Couldn't pull some images: %s\n", err)
		err = nil
	}
	for _, specTemplate := range specs {
		spec := getSpecGen(specTemplate)
		err = spec.Validate()
		if err != nil {
			err = fmt.Errorf("error generating spec for %s: %w", specTemplate.Name, err)
			break
		}
		if len(specTemplate.Volumes) > 0 {
			if err = c.CreateAllVolumes(specTemplate.Volumes); err != nil {
				err = fmt.Errorf("error creating volumes: %w", err)
				return err
			}
		}
		res, err := containers.CreateWithSpec(c.connCtx, spec)
		if err != nil {
			err = fmt.Errorf("error creating container %s: %w", specTemplate.Name, err)
			return err
		}
		log := lgr.Create(specTemplate.Name, nil, nil)
		// Don't hide warnings while creating containers
		for _, warn := range res.Warnings {
			log.Warn(warn)
		}
	}
	return err
}

func getSpecGen(from config.ContainerConfig) *specgen.SpecGenerator {
	var mappings []specgen.PortMapping
	if len(from.PortMappings) != 0 {
		mappings = from.PortMappings.GetSpec()
	}
	return &specgen.SpecGenerator{
		ContainerBasicConfig: specgen.ContainerBasicConfig{
			Name:       from.Name,
			Pod:        from.Pod,
			Entrypoint: from.Entrypoint,
			Command:    from.Command,
			Env:        from.Env,
			Hostname:   from.Hostname,
			Remove:     from.Remove,
		},
		ContainerStorageConfig: specgen.ContainerStorageConfig{
			Image:   from.Image,
			Volumes: from.Volumes,
		},
		ContainerNetworkConfig: specgen.ContainerNetworkConfig{
			PortMappings:        mappings,
			PublishExposedPorts: from.PublishExposedPorts,
		},
	}
}
