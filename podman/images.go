package podman

import (
	"fmt"

	cc "github.com/containers/common/pkg/config"
	"github.com/containers/podman/v2/pkg/bindings/images"
	"github.com/containers/podman/v2/pkg/domain/entities"

	"gitlab.com/mrvik/podman-manager/config"
)

func (c *Manager) PullImages(specs []config.ContainerConfig) (err error) {
	dedupImages := make(map[string]struct{})
	for _, spec := range specs {
		dedupImages[spec.Image] = struct{}{}
	}
	for image := range dedupImages {
		_, err = images.Pull(c.connCtx, image, entities.ImagePullOptions{
			// Refresh image also if it exists
			PullPolicy: cc.PullImageAlways,
		})
		if err != nil {
			err = fmt.Errorf("error pulling image %s: %w", image, err)
			break
		}
	}
	return
}
