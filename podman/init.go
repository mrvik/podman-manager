package podman

import (
	"context"
	"fmt"

	"github.com/containers/podman/v2/pkg/bindings"

	"gitlab.com/mrvik/podman-manager/config"
)

type actionEntry struct {
	arg    []string
	action func([]string) error
}

type Manager struct {
	connCtx context.Context
}

func NewManager(ctx context.Context, socket string) (chk *Manager, err error) {
	connCtx, err := bindings.NewConnection(ctx, socket)
	if err != nil {
		err = fmt.Errorf("error creating connection to %s: %w", socket, err)
		return
	}
	chk = &Manager{
		connCtx: connCtx,
	}
	return
}

func (c *Manager) DoContainerActions(conf map[string]*config.ContainerConfig) (err error) {
	containerNames := make([]string, 0, len(conf))
	for name := range conf {
		conf[name].Name = name
		containerNames = append(containerNames, name)
	}
	toStart, toCreate, toResume, err := c.FilterActions(containerNames)
	if err != nil {
		return
	}
	// Easy tasks first
	actions := []actionEntry{
		{
			toResume, c.UnpauseContainers,
		},
		{
			toStart, c.StartContainers,
		},
	}
	for i, entry := range actions {
		err = entry.action(entry.arg)
		if err != nil {
			err = fmt.Errorf("action %d: %w\n", i, err)
			return
		}
	}
	specsToCreate := make([]config.ContainerConfig, len(toCreate))
	for i, containerName := range toCreate {
		specsToCreate[i] = *conf[containerName]
	}
	err = c.CreateContainers(specsToCreate)
	return
}
