package podman

import (
	"github.com/containers/podman/v2/pkg/bindings/volumes"
	"github.com/containers/podman/v2/pkg/domain/entities"
	"github.com/containers/podman/v2/pkg/specgen"
)

func (m *Manager) CreateAllVolumes(volumes []*specgen.NamedVolume) error {
	for _, volume := range volumes {
		switch exists, err := m.FindVolume(volume.Name); true {
		case err != nil:
			return err
		case exists:
			continue
		}
		if err := m.CreateVolume(volume.Name); err != nil {
			return err
		}
	}
	return nil
}

func (m *Manager) CreateVolume(name string) error {
	_, err := volumes.Create(m.connCtx, entities.VolumeCreateOptions{
		Name: name,
	})
	return err
}

func (m *Manager) FindVolume(name string) (exists bool, err error) {
	list, err := volumes.List(m.connCtx, map[string][]string{
		"name": {name},
	})
	if err != nil {
		return
	}

	for _, spec := range list {
		if spec.Name == name {
			exists = true
			break
		}
	}

	return
}
